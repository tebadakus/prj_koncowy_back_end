/**
 * 
 */
package Main;

/**
 * @author adakus
 * Obsługa relacyjnej bazy danych za pomocą hibernate.
 * Program wykorzystuje funkcje takie jak: 
 * zapisz-save, uzupełnij-update, odczytaj-read, usuń-delete, pokaż tabelę-get all.
 *
 */

import java.util.Scanner;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import driver.map.*;
import hibernate.maps.*;

// TODO: Auto-generated Javadoc
/**
 * The Class Menu.
 */
public class Menu {

	/** The input from scanner int. */
	private static int inputFromScannerInt;
	
	/** The polaczenie hibernate. */
	private static SessionFactory polaczenieHibernate;

	/**
	 * Wywołuje Scanner i wczytuje String ze Scanner'a.
	 *
	 * @return the string
	 */
	static String inputScannerStrReturn() {
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		return input.nextLine();
	}

	/**
	 * Wywołuje Scanner i wczytuje int ze Scanner'a.
	 *
	 * @return the int
	 */
	static int inputScannerIntReturn() {
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		return input.nextInt();
	}

	/**
	 * Wywołuje Scanner i wczytuje int ze Scanner'a.
	 */
	static void inputScannerInt() {
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		inputFromScannerInt = input.nextInt();
	}

	/**
	 * Ustawienie połączenia hibernate do bazy.
	 */
	protected void setup() {
		final StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
		// configures setings from hibernate.cfg.xml
		try {
			polaczenieHibernate = new MetadataSources(registry).buildMetadata().buildSessionFactory();
		} catch (Exception ex) {
			StandardServiceRegistryBuilder.destroy(registry);
			throw new RuntimeException(ex);
		}
	}

	/**
	 * Zamknięcie połączenia hibernate do bazy.
	 */
	protected void exit() {
		if (polaczenieHibernate != null)
			polaczenieHibernate.close();
		println("Zakonczenie");
	}

	/**
	 * Utworzenie połączenia do bazy przez hibernate.
	 *
	 * @return the polaczenie
	 */
	protected SessionFactory getPolaczenie() {
		return polaczenieHibernate;
	}

	/**
	 * println - wyświetla przekazany obiekt w nowej linii.
	 *
	 * @param o the o
	 */
	void println(Object o) {
		System.out.println(o);
	}

	/**
	 * SplitStringWithComa() - dokonuje podziału po przecinku, wprowadzonego w
	 * skanerze stringa. Zwraca tablice.
	 *
	 * @return the string[]
	 */
	String[] SplitStringWithComa() {
		String tmp[] = inputScannerStrReturn().split(","); // podział po przecinku
		for (@SuppressWarnings("unused")
		String val : tmp)
			;
		return tmp;
	}

	/**
	 * menu2 - Wyświetla dostępne operacje możliwe do wykonania na wybranej tabeli
	 * lub wyjść do menu głównego (wybór tabeli).
	 * 
	 */
	void menu2() {
		println("----------------------------------\n" + "Wybierz opcję wpisując cyfrę: \n"
				+ "1. Save    - dodaje do bazy\n" + "2. Update  - Uzupełnia, poprawia wpis do bazy\n"
				+ "3. Read    - Odczytuje, wyszukuje dane w bazie\n" + "4. Delete  - Usuwa wiersz z tabeli\n"
				+ "5. Get_all - Odczytuje wszystkie dane z tabeli\n" + "99.Menu główne - Wybór Tabeli");
	}

	/**
	 * menu1 - Pozwala wybrać tabelę z którą chce się pracować. Po wyborze tabeli
	 * tworzy obiekt do ustanowienia połączenia (sesji) Wybierając 0 można zakończyć
	 * działanie programu.
	 * 
	 */

	void menu1() {

		Menu hibernate = new Menu();

		hibernate.setup();

		do {
			println("Operacje na bazie danych. Wybierz Tabelę: \n" + "------------------------------------------\n"
					+ "1. tabela - Uzytkownicy\n" + "2. tabela - Kursy\n" + "3. tabela - Zasoby (treść kursów)\n"
					+ "4. tabela - Autorzy\n" + "5. tabela - Rodzaje_Kursow\n" + "6. tabela - Poziom_Kursow\n"
					+ "0. Zakończ program !!!");

			try {

				println("Wybierz Tabelę : ");

				inputScannerInt();
				switch (inputFromScannerInt) {

// -UZYTKOWNICY--------------------------------------------------------------------
				case 1:
					println("Wybrano: 1. tabela - Uzytkownicy");
					UzytkownicyMapRun uzytkownicyMrun = new UzytkownicyMapRun(hibernate.getPolaczenie().openSession());

					do {
						menu2();
						try {

							inputScannerInt();
							switch (inputFromScannerInt) {
// -------------Dodaj----------------------------------------------------------
							case 1:
								println("Wybrano dodawanie uzytkownika do tabeli, \n"
										+ "Wpisz kolejno potrzebne dane oddzielając je przecinkami:\n"
										+ "Name Surname,  email,  phone_number,  login,  password,  idCourse");
								String tmp[] = SplitStringWithComa();
								for (@SuppressWarnings("unused")
								String val : tmp)
									;
								uzytkownicyMrun.save(tmp[0], tmp[1], tmp[2], tmp[3], tmp[4], Long.valueOf(tmp[5]));
								break;
// -------------Uzupełnij----------------------------------------------------------
							case 2:
								int wybierz = 0;
								boolean breakLoop = true;
								String nazwaKolumny = null;
								String wiersz = null;
								println("Wybrano poprawianie/uzupełnianie danych w tabeli 'Uzytkownicy', \n");
								println("Wybież kolumne docelową: \n" + "1 - id_kursu \n" + "2 - Pozostałe kolumny: "
										+ "imie_nazwisko; email; telefon; login");
								do {
									wybierz = inputScannerIntReturn();
									if (wybierz == 1) {
										println("Podaj uzytkownika do, którego jest przypisany kurs: ");
										wiersz = inputScannerStrReturn();
										println("Wpisz kolejno potrzebne dane oddzielając je przecinkami:\n"
												+ " Stare Id,Nowe Id");
										String tmpUpd[] = SplitStringWithComa();
										for (@SuppressWarnings("unused")
										String val : tmpUpd)
											;

										uzytkownicyMrun.update("id_kursu", wiersz, Long.valueOf(tmpUpd[0]),
												Long.valueOf(tmpUpd[1]));
										breakLoop = false;
									} else if (wybierz == 2) {
										println("Wybież rodzaj kolumny docelowej: \n" + "1 - imie_nazwisko\n"
												+ "2 - email\n" + "3 - telefon\n" + "4 - login");
										do {
											println("Podaj nr kolumny: ");
											wybierz = inputScannerIntReturn();

											switch (wybierz) {
											case 1:
												nazwaKolumny = "imie_nazwisko";
												breakLoop = false;
												break;
											case 2:
												nazwaKolumny = "email";
												breakLoop = false;
												break;
											case 3:
												nazwaKolumny = "telefon";
												breakLoop = false;
												break;
											case 4:
												nazwaKolumny = "login";
												breakLoop = false;
												break;
											default:
												println("Nie ma takiej kolumny !");
											}
										} while (breakLoop);

										println("Wpisz kolejno potrzebne dane do uzupełnienia oddzielając je przecinkami:\n"
												+ " Stare,Nowe ");
										String tmpUpd[] = SplitStringWithComa();
										for (@SuppressWarnings("unused")
										String val : tmpUpd)
											;
										uzytkownicyMrun.update(nazwaKolumny, tmpUpd[0], tmpUpd[1]);

									} else {
										println("Podałeś niewłaściwe dane, spróbuj jeszcze raz!:");
									}
								} while (breakLoop);

								break;

// -------------Odczytaj----------------------------------------------------------
							case 3:
								println("Wybrano odczyt uzytkownika z tabeli, \n"
										+ "1 - Podaj imie i nazwisko uzytkownika\n" + "2 - Podaj Id uzytkownika");
								long id = 0;
								wybierz = 0;
								breakLoop = true;
								do {
									wybierz = inputScannerIntReturn();
									if (wybierz == 1) {
										println("Podaj imie i nazwisko uzytkownika: ");
										String nazwa = inputScannerStrReturn();
										id = uzytkownicyMrun.read(nazwa).getId();
										breakLoop = false;
									} else if (wybierz == 2) {
										println("Podaj Id uzytkownika: ");
										id = inputScannerIntReturn();
										breakLoop = false;
									} else {
										println("Błędny wybór ! spróbuj jeszcze raz: ");
									}
								} while (breakLoop);
								println("ID: " + uzytkownicyMrun.read(id).getId() + ", Uzytkownik: "
										+ uzytkownicyMrun.read(id).getUsrNameSurname() + ", email:" + " "
										+ uzytkownicyMrun.read(id).getEmail() + ", telefon: "
										+ uzytkownicyMrun.read(id).getPhone() + ", login: "
										+ uzytkownicyMrun.read(id).getLogin());
								break;
// -------------Usuń----------------------------------------------------------
							case 4:
								println("Wybrano usunięcie uzytkownika z tabeli\n"
										+ "1 - Podaj imie i nazwisko uzytkownika\n" + "2 - Podaj Id uzytkownika");
								id = 0;
								wybierz = 0;
								breakLoop = true;

								do {
									wybierz = inputScannerIntReturn();
									if (wybierz == 1) {
										println("Podaj imie i nazwisko uzytkownika: ");
										id = uzytkownicyMrun.read(inputScannerStrReturn()).getId();

										breakLoop = false;
									} else if (wybierz == 2) {
										println("Podaj Id uzytkownika: ");
										id = inputScannerIntReturn();
										breakLoop = false;
									} else {
										println("Błędny wybór ! spróbuj jeszcze raz: ");
									}
								} while (breakLoop);
								println("Jesteś pewien? Chcesz usunąć uzytkownika "
										+ uzytkownicyMrun.read(id).getUsrNameSurname() + "? Y , n");
								if (inputScannerStrReturn().equals("Y"))
									uzytkownicyMrun.delete(id); // uzytkownika zawsze mozna usunac
								break;
// -------------Odczytaj-Tabelę----------------------------------------------------------
							case 5:
								for (Uzytkownicy element : uzytkownicyMrun.getAll()) {
									println(element.getId() + ", Uzytkownik: " + element.getUsrNameSurname()
											+ ", email: " + " " + element.getEmail() + ", telefon: "
											+ element.getPhone() + ", Id kursu wybranego: " + element.getIdCourse()
											+ ", Login: " + element.getLogin());
								}
								break;
							}

						} catch (Exception e) {
							System.out.println("Błąd " + e);
						}
					} while (inputFromScannerInt != 99);

					break;
// -KURSY--------------------------------------------------------------------------------
				case 2:
					println("Wybrano: 2. tabela - Kursy");
					KursyMapRun kursyMrun = new KursyMapRun(hibernate.getPolaczenie().openSession());

					do {
						menu2();
						try {

							inputScannerInt();
							switch (inputFromScannerInt) {
// -------------Dodaj----------------------------------------------------------
							case 1:
								println("Wybrano dodawanie kursu do tabeli, \n"
										+ "Wpisz kolejno potrzebne dane oddzielając je przecinkami:\n"
										+ " Course Name,  idCourseType,  idCourseLevel,  idAuthor,  idAssets");
								String tmp[] = SplitStringWithComa();
								for (@SuppressWarnings("unused")
								String val : tmp)
									;
								kursyMrun.save(tmp[0], Long.valueOf(tmp[1]), Long.valueOf(tmp[2]), Long.valueOf(tmp[3]),
										Long.valueOf(tmp[4]));
								break;
// -------------Uzupełnij----------------------------------------------------------
							case 2:
								int wybierz = 0;
								boolean breakLoop = true;
								String nazwaKolumny = null;
								String wiersz = null;
								println("Wybrano poprawianie/uzupełnianie danych w tabeli, \n");
								println("Wybież kolumne docelową: \n" + "1 - nazwa_kursu\n" + "2 - Pozostałe kolumny: "
										+ "id_rodzaj_kursu; id_poziom_kursu; id_autor; id_zasob");
								do {
									wybierz = inputScannerIntReturn();
									if (wybierz == 1) {
										println("Wpisz kolejno potrzebne dane oddzielając je przecinkami:\n"
												+ " Stara nazwa,  Nowa nazwa");
										String tmpUpd[] = SplitStringWithComa();
										for (@SuppressWarnings("unused")
										String val : tmpUpd)
											;
										kursyMrun.update("nazwa_kursu", tmpUpd[0], tmpUpd[1]);
										breakLoop = false;
									} else if (wybierz == 2) {
										println("Wybież rodzaj kolumny docelowej: \n" + "1 - id_rodzaj_kursu\n"
												+ "2 - id_poziom_kursu\n" + "3 - id_autor\n" + "4 - id_zasob");
										do {
											println("Podaj nr kolumny: ");
											wybierz = inputScannerIntReturn();

											switch (wybierz) {
											case 1:
												nazwaKolumny = "id_rodzaj_kursu"; // "courseName"; //
												breakLoop = false;
												break;
											case 2:
												nazwaKolumny = "id_poziom_kursu"; // "idCourseLevel"; //
												breakLoop = false;
												break;
											case 3:
												nazwaKolumny = "id_autor"; // "idAuthor"; //
												breakLoop = false;
												break;
											case 4:
												nazwaKolumny = "id_zasob"; // "idAssets"; //
												breakLoop = false;
												break;
											default:
												println("Nie ma takiej kolumny !");
											}
										} while (breakLoop);
										println("Podaj nazwę kursu: ");
										wiersz = inputScannerStrReturn();
										println("Wpisz kolejno potrzebne dane do uzupełnienia oddzielając je przecinkami:\n"
												+ " Stare,Nowe ");
										String tmpUpd[] = SplitStringWithComa();
										for (@SuppressWarnings("unused")
										String val : tmpUpd)
											;
										kursyMrun.update(nazwaKolumny, wiersz, Long.valueOf(tmpUpd[0]),
												Long.valueOf(tmpUpd[1]));
									} else {
										println("Podałeś niewłaściwe dane, spróbuj jeszcze raz!:");
									}
								} while (breakLoop);
								break;
// -------------Odczytaj----------------------------------------------------------
							case 3:
								ZasobyMapRun zasobyMrun = new ZasobyMapRun(hibernate.getPolaczenie().openSession());
								RodzajeKursowMapRun rodzajKursuMrun = new RodzajeKursowMapRun(
										hibernate.getPolaczenie().openSession());
								PoziomKursowMapRun poziomKursuMrun = new PoziomKursowMapRun(
										hibernate.getPolaczenie().openSession());
								AutorzyMapRun autorzyMrun = new AutorzyMapRun(hibernate.getPolaczenie().openSession());

								println("Wybrano odczyt kursu z tabeli, \n" + "1 - Podaj nazwe kursu: "
										+ "2 - Podaj Id kursu: ");
								long id = 0;
								wybierz = 0;
								breakLoop = true;
								do {
									wybierz = inputScannerIntReturn();
									if (wybierz == 1) {
										println("Podaj nazwe kursu: ");
										id = kursyMrun.read(inputScannerStrReturn()).getId();
										breakLoop = false;
									} else if (wybierz == 2) {
										println("Podaj Id kursu: ");
										id = inputScannerIntReturn();
										breakLoop = false;
									} else {
										println("Błędny wybór ! spróbuj jeszcze raz: ");
									}
								} while (breakLoop);
								println("ID: " + kursyMrun.read(id).getId() + ", Nazwa kursu: "
										+ kursyMrun.read(id).getCourseName() + ", Data dodania:"
										+ kursyMrun.read(id).getDateAdded() + ", Autor: "
										+ autorzyMrun.read(kursyMrun.read(id).getIdAuthor()).getNameSurname()
										+ ", Pozom kursu: "
										+ poziomKursuMrun.read(kursyMrun.read(id).getIdCourseLevel()).getLevel()
										+ ",  Typ kursu: "
										+ rodzajKursuMrun.read(kursyMrun.read(id).getIdCourseType()).getCourseType()
										+ ", Treść artykułu:"
										+ zasobyMrun.read(kursyMrun.read(id).getIdAssets()).getAsset());
								break;
// -------------Usuń----------------------------------------------------------
							case 4:
								println("Wybrano usunięcie kursu z tabeli\n" + "1 - Podaj tytuł kursu\n"
										+ "2 - Podaj Id kursu");

								id = 0;
								wybierz = 0;
								breakLoop = true;

								do {
									wybierz = inputScannerIntReturn();
									if (wybierz == 1) {
										println("Podaj tytuł kursu: ");
										id = kursyMrun.read(inputScannerStrReturn()).getId();

										breakLoop = false;
									} else if (wybierz == 2) {
										println("Podaj Id kursu: ");
										id = inputScannerIntReturn();
										breakLoop = false;
									} else {
										println("Błędny wybór ! spróbuj jeszcze raz: ");
									}
								} while (breakLoop);
								println("Jesteś pewien? Chcesz usunąć kurs '" + kursyMrun.read(id).getCourseName()
										+ "'? Y , n");
								if (inputScannerStrReturn().equals("Y"))

									try {
										kursyMrun.delete(id);
									} catch (Exception e) {
										println("Nie można usunąć, element jest używany: " + e);
									}
								break;
// -------------Odczytaj-Tabelę----------------------------------------------------------
							case 5:
								for (Kursy element : kursyMrun.getAll()) {
									println(element.getId() + ", Nazwa kursu: " + element.getCourseName() + ""
											+ ", Data dodania: " + element.getDateAdded() + ", Id rodzaj kursu: "
											+ element.getIdCourseType() + "" + ", Id poziomu kursu: "
											+ element.getIdCourseLevel() + ", Id autora: " + element.getIdAuthor() + ""
											+ ", Id zasobu: " + element.getIdAssets());
								}
								break;
							}

						} catch (Exception e) {
							System.out.println("Błąd " + e);

						}
					} while (inputFromScannerInt != 99);

					break;
// -ZASOBY--------------------------------------------------------------------------------
				case 3:
					println("Wybrano: 3. tabela - Zasoby (treść kursów)");

					ZasobyMapRun zasobyMrun = new ZasobyMapRun(hibernate.getPolaczenie().openSession());

					do {
						menu2();
						try {

							inputScannerInt();
							switch (inputFromScannerInt) {
// -------------Dodaj----------------------------------------------------------
							case 1:
								println("Wybrano dodawanie zasobu do tabeli, \n" + "Wpisz tresc kursu (link):\n");
								zasobyMrun.save(inputScannerStrReturn());
								break;
// -------------Uzupełnij----------------------------------------------------------
							case 2:
								println("Wybrano poprawianie/uzupełnianie danych w tabeli, \n"
										+ "Wpisz kolejno potrzebne dane oddzielając je przecinkami:\n"
										+ " Star zasób,  Nowy zasób");
								String tmpUpd[] = SplitStringWithComa();
								for (@SuppressWarnings("unused")
								String val : tmpUpd)
									;
								zasobyMrun.update(tmpUpd[0], tmpUpd[1]);
								break;
// -------------Odczytaj----------------------------------------------------------
							case 3:
								println("Wybrano odczyt zasobu-treści kursu z tabeli, \n"
										+ "1. Podaj nazwe zasobu np.(plik.pdf)\n" + "2- Podaj Id zasobu");

								long id = 0;
								int wybierz = 0;
								boolean breakLoop = true;
								do {
									wybierz = inputScannerIntReturn();
									if (wybierz == 1) {
										println("Podaj nazwe zasobu np.(plik.pdf): ");
										id = zasobyMrun.read(inputScannerStrReturn()).getId();
										breakLoop = false;
									} else if (wybierz == 2) {
										println("Podaj Id zasobu: ");
										id = inputScannerIntReturn();
										breakLoop = false;
									} else {
										println("Błędny wybór ! spróbuj jeszcze raz: ");
									}
								} while (breakLoop);
								println("ID: " + zasobyMrun.read(id).getId() + ", Zasob: "
										+ zasobyMrun.read(id).getAsset());

								break;
// -------------Usuń----------------------------------------------------------
							case 4:
								println("Wybrano usunięcie zasobu z tabeli\n" + "1 - Podaj nazwę zasobu\n"
										+ "2 - Podaj Id zasobu");

								id = 0;
								wybierz = 0;
								breakLoop = true;

								do {
									wybierz = inputScannerIntReturn();
									if (wybierz == 1) {
										println("Podaj nazwę zasobu: ");
										id = zasobyMrun.read(inputScannerStrReturn()).getId();

										breakLoop = false;
									} else if (wybierz == 2) {
										println("Podaj Id zasobu: ");
										id = inputScannerIntReturn();
										breakLoop = false;
									} else {
										println("Błędny wybór ! spróbuj jeszcze raz: ");
									}
								} while (breakLoop);
								println("Jesteś pewien? Chcesz usunąć zasób '" + zasobyMrun.read(id).getAsset()
										+ "'? Y , n");
								if (inputScannerStrReturn().equals("Y"))
									try {
										zasobyMrun.delete(id);
									} catch (Exception e) {
										println("Nie można usunąć, element jest używany: " + e);
									}
								break;
// -------------Odczytaj-Tabelę----------------------------------------------------------
							case 5:
								for (Zasoby element : zasobyMrun.getAll()) {
									println((element.getId() + ", Treść kursu: " + element.getAsset()));
								}
								break;
							}

						} catch (Exception e) {
							println("Błąd " + e);
						}
					} while (inputFromScannerInt != 99);

					break;
// -AUTORZY------------------------------------------------------------------------------
				case 4:
					println("Wybrano: 4. tabela - Autorzy");
					AutorzyMapRun autorzyMrun = new AutorzyMapRun(hibernate.getPolaczenie().openSession());
					do {
						menu2();
						try {

							inputScannerInt();
							switch (inputFromScannerInt) {
// -------------Dodaj-----------------------------------------------------------
							case 1:
								println("Wybrano dodawanie autora do tabeli, \n" + "Wpisz imie i nazwisko autora:\n"
										+ " Name Surname");
								autorzyMrun.save(inputScannerStrReturn());
								break;
// -------------Uzupełnij-----------------------------------------------------------
							case 2:
								println("Wybrano poprawianie/uzupełnianie danych w tabeli, \n"
										+ "Wpisz kolejno potrzebne dane oddzielając je przecinkami:\n"
										+ " Stary autor,  Nowy autor");
								String tmpUpd[] = SplitStringWithComa();
								for (@SuppressWarnings("unused")
								String val : tmpUpd)
									;
								autorzyMrun.update(tmpUpd[0], tmpUpd[1]);
								break;
// -------------Odczytaj-----------------------------------------------------------
							case 3:
								println("Wybrano odczyt autora z tabeli, \n" + "1 - Podaj imie nazwisko autora\n"
										+ "2 - Podaj Id autora");
								long id = 0;
								int wybierz = 0;
								boolean breakLoop = true;
								do {
									wybierz = inputScannerIntReturn();
									if (wybierz == 1) {
										println("Podaj imię i nazwisko autora np. 'Adam Mickiewicz': ");
										id = autorzyMrun.read(inputScannerStrReturn()).getId();
										breakLoop = false;
									} else if (wybierz == 2) {
										println("Podaj Id autora: ");
										id = inputScannerIntReturn();
										breakLoop = false;
									} else {
										println("Błędny wybór ! spróbuj jeszcze raz: ");
									}
								} while (breakLoop);
								println("ID: " + autorzyMrun.read(id).getId() + ", Imię i nazwisko autora: "
										+ autorzyMrun.read(id).getNameSurname());
								break;
// -------------Usuń-----------------------------------------------------------
							case 4:
								println("Wybrano usunięcie autora z tabeli\n" + "1 - Podaj imię i nazwisko autora\n"
										+ "2 - Podaj Id autora");
								id = 0;
								wybierz = 0;
								breakLoop = true;
								do {
									wybierz = inputScannerIntReturn();
									if (wybierz == 1) {
										println("Podaj imię i nazwisko autora: ");
										id = autorzyMrun.read(inputScannerStrReturn()).getId();

										breakLoop = false;
									} else if (wybierz == 2) {
										println("Podaj Id autora: ");
										id = inputScannerIntReturn();
										breakLoop = false;
									} else {
										println("Błędny wybór ! spróbuj jeszcze raz: ");
									}
								} while (breakLoop);
								println("Jesteś pewien? Chcesz usunąć autora '" + autorzyMrun.read(id).getNameSurname()
										+ "'? Y , n");
								if (inputScannerStrReturn().equals("Y"))
									try {
										autorzyMrun.delete(id);
									} catch (Exception e) {
										println("Nie można usunąć, element jest używany: " + e);
									}
								break;
// -------------Odczytaj-Tabelę----------------------------------------------------------
							case 5:
								for (Autorzy element : autorzyMrun.getAll()) {
									println("Autor: " + element.getId() + ", " + element.getNameSurname());
								}
								break;
							}

						} catch (Exception e) {
							println("Błąd " + e);
						}
					} while (inputFromScannerInt != 99);

					break;
// -RODZAJE-KURSÓW----------------------------------------------------------------------
				case 5:
					println("Wybrano: 5. tabela - Rodzaje_Kursow");
					RodzajeKursowMapRun rodzajKursuMrun = new RodzajeKursowMapRun(
							hibernate.getPolaczenie().openSession());

					do {
						menu2();
						try {

							inputScannerInt();
							switch (inputFromScannerInt) {
// -------------Odczytaj-Tabelę----------------------------------------------------------
							case 1:
								println("Wybrano dodawanie rodzaju kursu do tabeli, \n" + "Wpisz rodzaj kursu: ");

								rodzajKursuMrun.save(inputScannerStrReturn());
								break;
// -------------Uzupełnij----------------------------------------------------------------
							case 2:
								println("Wybrano poprawianie/uzupełnianie danych w tabeli, \n"
										+ "Wpisz kolejno potrzebne dane oddzielając je przecinkami:\n"
										+ " Stary rodzaj krsu,  Nowy rodzaj kursu");
								String tmpUpd[] = SplitStringWithComa();
								for (@SuppressWarnings("unused")
								String val : tmpUpd)
									;
								println("Update OK");
								rodzajKursuMrun.update(tmpUpd[0], tmpUpd[1]);
								break;
// -------------Odczytaj----------------------------------------------------------------
							case 3:
								println("Wybrano odczyt rodzaju kursu z tabeli, \n" + "1 - Podaj rodzaj kursu\n"
										+ "2 - Podaj Id rodzaju kursu");
								long id = 0;
								int wybierz = 0;
								boolean breakLoop = true;
								do {
									wybierz = inputScannerIntReturn();
									if (wybierz == 1) {
										println("Podaj poziom kursu: ");
										id = rodzajKursuMrun.read(inputScannerStrReturn()).getId();
										breakLoop = false;
									} else if (wybierz == 2) {
										println("Podaj Id poziomu kursu: ");
										id = inputScannerIntReturn();
										breakLoop = false;
									} else {
										println("Błędny wybór ! spróbuj jeszcze raz: ");
									}
								} while (breakLoop);
								println("ID: " + rodzajKursuMrun.read(id).getId() + ", Rodzaj kursu: "
										+ rodzajKursuMrun.read(id).getCourseType());
								break;
// ------------Usuń-------------------------------------------------------------------
							case 4:
								println("Wybrano usunięcie rodzaju kursu z tabeli\n" + "1 - Podaj rodzaj kursu\n"
										+ "2 - Podaj Id rodzaju kursu");
								id = 0;
								wybierz = 0;
								breakLoop = true;

								do {
									wybierz = inputScannerIntReturn();
									if (wybierz == 1) {
										println("Podaj rodzaj kursu: ");
										id = rodzajKursuMrun.read(inputScannerStrReturn()).getId();

										breakLoop = false;
									} else if (wybierz == 2) {
										println("Podaj Id rodzaju kursu: ");
										id = inputScannerIntReturn();
										breakLoop = false;
									} else {
										println("Błędny wybór ! spróbuj jeszcze raz: ");
									}
								} while (breakLoop);
								println("Jesteś pewien? Chcesz usunąć rodzaj kursu '"
										+ rodzajKursuMrun.read(id).getCourseType() + "'? Y , n");
								if (inputScannerStrReturn().equals("Y"))
									try {
										rodzajKursuMrun.delete(id);
									} catch (Exception e) {
										println("Nie można usunąć, element jest używany: " + e);
									}
								break;
// ------------Odczytaj-Tabelę------------------------------------------------------------
							case 5:
								for (RodzajeKursow element : rodzajKursuMrun.getAll()) {
									println(element.getId() + ", Rodzaj kursu: " + element.getCourseType());
								}
								break;
							}

						} catch (Exception e) {
							println("Błąd " + e);
						}
					} while (inputFromScannerInt != 99);

					break;
// -POZIOMY-KURSOW------------------------------------------------------------------------
				case 6:
					println("Wybrano: 6. tabela - Poziom_Kursow");
					PoziomKursowMapRun poziomKursuMrun = new PoziomKursowMapRun(
							hibernate.getPolaczenie().openSession());

					do {
						menu2();
						try {

							inputScannerInt();
							switch (inputFromScannerInt) {
// -------------Dodaj----------------------------------------------------------------
							case 1:
								println("Wybrano dodawanie poziomu kursu do tabeli, \n" + "Wpisz poziom kursu:\n"
										+ " Level");
								poziomKursuMrun.save(inputScannerStrReturn());
								break;
// -------------Uzupełnij----------------------------------------------------------------
							case 2:
								println("Wybrano poprawianie/uzupełnianie danych w tabeli, \n"
										+ "Wpisz kolejno potrzebne dane oddzielając je przecinkami:\n"
										+ " Stary poziom krsu,  Nowy poziom kursu");
								String tmpUpd[] = SplitStringWithComa();
								for (@SuppressWarnings("unused")
								String val : tmpUpd)
									;
								println("Update OK");
								poziomKursuMrun.update(tmpUpd[0], tmpUpd[1]);

								break;
// -------------Odczytaj----------------------------------------------------------------
							case 3:
								println("Wybrano odczyt poziomu kursu z tabeli, \n" + "1 - Podaj poziom kursu\n"
										+ "2 - Podaj Id poziomu kursu");
								long id = 0;
								int wybierz = 0;
								boolean breakLoop = true;
								do {
									wybierz = inputScannerIntReturn();
									if (wybierz == 1) {
										println("Podaj poziom kursu: ");
										id = poziomKursuMrun.read(inputScannerStrReturn()).getId();
										breakLoop = false;
									} else if (wybierz == 2) {
										println("Podaj Id poziomu kursu: ");
										id = inputScannerIntReturn();
										breakLoop = false;
									} else {
										println("Błędny wybór ! spróbuj jeszcze raz: ");
									}
								} while (breakLoop);
								println("ID: " + poziomKursuMrun.read(id).getId() + ", Poziom kursu: "
										+ poziomKursuMrun.read(id).getLevel());
								break;
// -------------Usuń----------------------------------------------------------------
							case 4:
								println("Wybrano usunięcie poziom kursu z tabeli\n" + "1 - Podaj poziom kursu\n"
										+ "2 - Podaj Id poziomu kursu");
								id = 0;
								wybierz = 0;
								breakLoop = true;

								do {
									wybierz = inputScannerIntReturn();
									if (wybierz == 1) {
										println("Podaj poziom kursu: ");
										id = poziomKursuMrun.read(inputScannerStrReturn()).getId();

										breakLoop = false;
									} else if (wybierz == 2) {
										println("Podaj Id poziomu kursu: ");
										id = inputScannerIntReturn();
										breakLoop = false;
									} else {
										println("Błędny wybór ! spróbuj jeszcze raz: ");
									}
								} while (breakLoop);
								println("Jesteś pewien? Chcesz usunąć poziom kursu '"
										+ poziomKursuMrun.read(id).getLevel() + "'? Y , n");
								if (inputScannerStrReturn().equals("Y"))
									try {
										poziomKursuMrun.delete(id);
									} catch (Exception e) {
										println("Nie można usunąć, element jest używany: " + e);
									}
								break;
// -------------Odczytaj-Tabelę----------------------------------------------------------
							case 5:
								for (PoziomKursow element : poziomKursuMrun.getAll()) {
									println(element.getId() + ", Poziom kursu: " + element.getLevel());
								}
								break;
							}

						} catch (Exception e) {
							println("Błąd " + e);
						}
					} while (inputFromScannerInt != 99);

					break;
// -ZAKOŃCZ------------------------------------------------------------------------------
				case 0:
					println("Zamykam program !!!");
					hibernate.exit();
					System.exit(0);
				default:
					println("Zły wybór !!!");
				}

			} catch (Exception e) {
				println("Błąd " + e);
			}
		} while (inputFromScannerInt != 0);

// ----------------------------------------------------------------------------------------
	}

	/**
	 * The main method.
	 *
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {

		System.out.println("Projekt Końcowy Hibernate-Maven 'Kursy' ");
		new Menu().menu1();

	}

}

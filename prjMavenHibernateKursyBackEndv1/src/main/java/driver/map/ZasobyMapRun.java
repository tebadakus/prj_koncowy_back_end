/*
 * 
 */
package driver.map;

/**
 * @author adakus
 *
 */

import java.math.BigInteger;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import org.hibernate.Session;
import hibernate.maps.*;

// TODO: Auto-generated Javadoc
/**
 * The Class ZasobyMapRun.
 */
public class ZasobyMapRun {

	/** The session. */
	private Session session;

	/**
	 * Instantiates a new zasoby map run.
	 *
	 * @param session the session
	 */
	public ZasobyMapRun(Session session) {
		this.session = session;
	}

	/**
	 * Metoda - Zapis Zasobu do bazy danych. Należy podać poniższy parametr.
	 *
	 * @param assets the assets
	 */
	public void save(String assets) {
		session.beginTransaction();
		Zasoby asset = new Zasoby();
		asset.setAsset(assets);
		session.save(asset);
		session.getTransaction().commit();
	}

	/**
	 * Metoda - Odczyt Zasobu po przypisanym id. Należy podać poniższy parametr.
	 *
	 * @param id the id
	 * @return the zasoby
	 */
	public Zasoby read(long id) {
		Zasoby zasob = session.get(Zasoby.class, id);
		return zasob;
	}

	/**
	 * Metoda - odczyt Zasobu po nazwie. Należy podać poniższy parametr.
	 *
	 * @param assets the assets
	 * @return the zasoby
	 */
	public Zasoby read(String assets) {
		@SuppressWarnings({ "unchecked" })
		List<Object> asset = session.createSQLQuery("SELECT id_zasob FROM zasoby WHERE zasob='" + assets + "'").list();
		if (asset.isEmpty())
			return new Zasoby();
		BigInteger bi = (BigInteger) asset.get(0);
		Zasoby zasob = session.get(Zasoby.class, bi.longValue());
		return zasob;
	}

	/**
	 * Metoda update - poprawia wpis do bazy bez podanego id. Podane są tylko
	 * poniższe parametry. Metoda wywołuje metode nie publiczną update,
	 * uwzględniającą id, przypisując do id=-1
	 *
	 * @param oldAssets the old assets
	 * @param newAssets the new assets
	 * @return true, if successful
	 */
	public boolean update(String oldAssets, String newAssets) {
		return update(oldAssets, newAssets, -1);
	}

	/**
	 * Metoda update - poprawia wpis do bazy. Wywoływana przy użyciu poniższych
	 * parametrów.
	 *
	 * @param oldAssets the old assets
	 * @param newAssets the new assets
	 * @param id the id
	 * @return true, if successful
	 */
	boolean update(String oldAssets, String newAssets, long id) {
		Zasoby zasob = (id != -1) ? read(id) : read(oldAssets);
		if (zasob == null)
			return false;
		System.out.println(zasob.getAsset());
		zasob.setAsset(newAssets);
		session.beginTransaction();
		session.update(zasob);
		session.getTransaction().commit();
		return true;
	}

	/**
	 * Metoda delete - Podany jest tylko jeden parametr (String). Wywołuje nie
	 * publiczną metodę przy użyciu podanego stringa i przypisując id=-1
	 *
	 * @param assets the assets
	 * @return true, if successful
	 */
	public boolean delete(String assets) {
		return delete(assets, -1);
	}

	/**
	 * Metoda delete - podany tylko jeden parametr (long id) Wywołuje nie publiczną
	 * metodę przy użyciu pustego stringa i podanego id.
	 *
	 * @param id the id
	 * @return true, if successful
	 */
	public boolean delete(long id) {
		return delete("", id);
	}

	/**
	 * Metoda delete - do której się odwołują wszystkie pozostałe. Metoda ta
	 * uwzględnia wszystkie możliwe parametry.
	 *
	 * @param assets the assets
	 * @param id the id
	 * @return true, if successful
	 */
	boolean delete(String assets, long id) {
		Zasoby zasob = (id != -1) ? read(id) : read(assets);
		if (zasob == null)
			return false;
		session.beginTransaction();
		session.delete(zasob);
		session.getTransaction().commit();
		return true;
	}

	/**
	 * Metoda -getAll powoduje utworzenie listy zawierającej wszystkie elementy
	 * odczytane z bazy danych. Lista zawiera wskazniki (adresy w pamięci) do
	 * poszczególnych objektów znajdujących się w liście.
	 * 
	 * Jeśli chcemy wyświetlić autorów należy użyć pętli np for.
	 * 
	 * Przykład: for (Zasoby element : zasobyMrun.getAll()) {
	 * System.out.println("Zasob: "+ element.getZasob());}
	 *
	 * @return the all
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Zasoby> getAll() {
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery criteria = builder.createQuery(Zasoby.class);
		criteria.from(Zasoby.class);
		List<Zasoby> zasob= session.createQuery(criteria).getResultList();
		return zasob;
	}

}

/**
 * 
 */
package driver.map;

/**
 * @author adakus
 *
 */

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import org.hibernate.Session;
import hibernate.maps.*;

// TODO: Auto-generated Javadoc
/**
 * The Class KursyMapRun.
 */
public class KursyMapRun {

	/** The session. */
	private Session session;

	/**
	 * Instantiates a new kursy map run.
	 *
	 * @param session the session
	 */
	public KursyMapRun(Session session) {
		this.session = session;
	}

	/**
	 * Metoda save, która wywoluje metodę save uwzględniającą date dodania dodając
	 * czas systmowy - LocalDateTime jako czas dodania.
	 *
	 * @param courseName the course name
	 * @param idCourseType the id course type
	 * @param idCourseLevel the id course level
	 * @param idAuthor the id author
	 * @param idAssets the id assets
	 * @return true, if successful
	 */
	public boolean save(String courseName, long idCourseType, long idCourseLevel, long idAuthor, long idAssets) {

		LocalDateTime currentDateTime = LocalDateTime.now();
		final DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
		String formattedDateTime = currentDateTime.format(formatter);
		// dodaje date systemowa jako date dodania
		return save(courseName, formattedDateTime, idCourseType, idCourseLevel, idAuthor, idAssets);
	}

	/**
	 * Metoda -Dodanie Kursu potrzebne są wszystkie poniższe parametry.
	 *
	 * @param courseName the course name
	 * @param dateAdded the date added
	 * @param idCourseType the id course type
	 * @param idCourseLevel the id course level
	 * @param idAuthor the id author
	 * @param idAssets the id assets
	 * @return true, if successful
	 */

	boolean save(String courseName, String dateAdded, long idCourseType, long idCourseLevel, long idAuthor,
			long idAssets) {
		session.beginTransaction();
		Kursy kurs = new Kursy();
		kurs.setCourseName(courseName);
		kurs.setDateAdded(dateAdded);
		kurs.setIdCourseType(idCourseType);
		kurs.setIdCourseLevel(idCourseLevel);
		kurs.setIdAuthor(idAuthor);
		kurs.setIdAssets(idAssets); // plik/link/materiał z tabeli 'Zasoby'

		session.save(kurs);
		session.getTransaction().commit();
		return true;
	}

	/**
	 * Metoda - Odczytu Kursu po przypisanym id. Należy podać poniższy parametr.
	 *
	 * @param id the id
	 * @return the kursy
	 */
	public Kursy read(long id) {
		Kursy kurs = session.get(Kursy.class, id);

		return kurs;
	}

	/**
	 * Metoda - odczyt Kursu po nazwie. Należy podać poniższy parametr.
	 *
	 * @param courseName the course name
	 * @return the kursy
	 */
	public Kursy read(String courseName) {
		@SuppressWarnings({ "unchecked" })
		List<Object> cours = session.createSQLQuery("SELECT id_kursu FROM kursy WHERE nazwa_kursu='" + courseName + "'")
				.list();
		if (cours.isEmpty())
			return new Kursy();
		
		BigInteger bi = (BigInteger) cours.get(0);
		
		Kursy kurs = session.get(Kursy.class, bi.longValue());
		return kurs;
	}

	/**
	 * Metoda update - poprawia wpis do bazy bez podanego id. Podane są tylko
	 * poniższe parametry. Metoda wywołuje metode update, uwzględniającą id,
	 * przypisując do id=-1
	 *
	 * @param column the column
	 * @param oldString the old string
	 * @param newString the new string
	 * @return true, if successful
	 */

	public boolean update(String column, String oldString, String newString) {
		return update(column, oldString, newString, -1);
	}

	/**
	 * Metoda update - poprawia wpis do bazy. Wywoływana przy użyciu poniższych
	 * parametrów.
	 *
	 * @param column    - Parametr okresla której kolumny dotyczy update. np:
	 *                  nazwa_kursu, data_dodania
	 * @param oldString the old string
	 * @param newString the new string
	 * @param id the id
	 * @return true, if successful
	 */
	boolean update(String column, String oldString, String newString, long id) {
		Kursy kurs = (id != -1) ? read(id) : read(oldString);
		if (kurs == null)
			return false;

		if (column.equals("nazwa_kursu")) {
			System.out.println(kurs.getCourseName());
			kurs.setCourseName(newString);
		}

		if (column.equals("data_dodania")) {
			System.out.println(kurs.getDateAdded());
			kurs.setDateAdded(newString);
		}

		session.beginTransaction();
		session.update(kurs);
		session.getTransaction().commit();
		return true;
	}

	/**
	 * Update.
	 *
	 * @param column the column
	 * @param row the row
	 * @param oldId the old id
	 * @param newId the new id
	 * @return true, if successful
	 */
	public boolean update(String column, String row, long oldId, long newId) {
		return update(column, row, oldId, newId, -1);
	}

	/**
	 * Update.
	 *
	 * @param column the column
	 * @param row the row
	 * @param oldId the old id
	 * @param newId the new id
	 * @param id the id
	 * @return true, if successful
	 */
	boolean update(String column, String row, long oldId, long newId, long id) {
		Kursy kurs = (id != -1) ? read(id) : read(row);
		if (kurs == null)
			return false;

		if (column.equals("id_zasob")) {
			System.out.println(kurs.getIdAssets());
			kurs.setIdAssets(newId);
		}

		if (column.equals("id_autor")) {
			System.out.println(kurs.getIdAuthor());
			kurs.setIdAuthor(newId);
		}

		if (column.equals("id_poziom_kursu")) {
			System.out.println(kurs.getIdCourseLevel());
			kurs.setIdCourseLevel(newId);
		}

		if (column.equals("id_rodzaj_kursu")) {
			System.out.println(kurs.getIdCourseType());
			kurs.setIdCourseType(newId);
		}

		session.beginTransaction();
		session.update(kurs);
		session.getTransaction().commit();
		return true;
	}

	/**
	 * Metoda delete - Podany jest tylko jeden parametr (String). Wywołuje metodę
	 * delete uwzględniającą dodatkowy parametr i przypisując do id=-1
	 *
	 * @param courseName the course name
	 * @return true, if successful
	 */

	public boolean delete(String courseName) {
		return delete(courseName, -1);
	}

	/**
	 * Metoda delete - podany tylko jeden parametr (long id) Wywołuje metodę delete
	 * uwzględniającą dodatkowy parametr i przypisując do niego pustego stringa i
	 * podane id.
	 *
	 * @param id the id
	 * @return true, if successful
	 */
	public boolean delete(long id) {
		return delete("", id);
	}

	/**
	 * Metoda delete - do której się odwołują wszystkie pozostałe metody delete.
	 * Metoda ta uwzględnia wszystkie możliwe parametry.
	 *
	 * @param courseName the course name
	 * @param id the id
	 * @return true, if successful
	 */
	boolean delete(String courseName, long id) {
		Kursy kurs = (id != -1) ? read(id) : read(courseName);
		if (kurs == null)
			return false;
		session.beginTransaction();
		session.delete(kurs);
		session.getTransaction().commit();
		return true;
	}

	/**
	 * Metoda -getAll powoduje utworzenie listy zawierającej wszystkie elementy
	 * odczytane z bazy danych. Lista zawiera wskazniki (adresy w pamięci) do
	 * poszczególnych objektów znajdujących się w liście.
	 * 
	 * Jeśli chcemy wyświetlić kursy należy użyć pętli np for.
	 * 
	 * Przykład: for (Kursy element : kursyMrun.getAll()) {
	 * System.out.println("Kurs: "+ element.getNameSurname());}
	 *
	 * @return the all
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Kursy> getAll() {
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery criteria = builder.createQuery(Kursy.class);
		criteria.from(Kursy.class);
		List<Kursy> kurs = session.createQuery(criteria).getResultList();
		return kurs;
	}

}

/*
 * 
 */
package driver.map;

import java.math.BigInteger;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import org.hibernate.Session;
import hibernate.maps.*;

// TODO: Auto-generated Javadoc
/**
 * The Class AutorzyMapRun.
 */
public class AutorzyMapRun {

	/** The session. */
	private Session session;

	/**
	 * Instantiates a new autorzy map run.
	 *
	 * @param session the session
	 */
	public AutorzyMapRun(Session session) {
		this.session = session;
	}

	/**
	 * Metoda - Zapis Autora do bazy danych. Należy podać poniższy parametr.
	 *
	 * @param nameSurname the name surname
	 */
	public void save(String nameSurname) {
		session.beginTransaction();
		Autorzy autor = new Autorzy();
		autor.setNameSurname(nameSurname);
		session.save(autor);
		session.getTransaction().commit();
	}

	/**
	 * Metoda - Odczyt Autora po przypisanym id. Należy podać poniższy parametr.
	 *
	 * @param id the id
	 * @return the autorzy
	 */
	public Autorzy read(long id) {
		Autorzy autor = session.get(Autorzy.class, id);
		return autor;
	}

	/**
	 * Metoda - odczyt Autora po imieniu i nazwisku. Należy podać poniższy parametr.
	 * Podajemy imie i nazwisko oddzielone spacją np. "Adam Mickiewicz"
	 *
	 * @param nameSurname the name surname
	 * @return the autorzy
	 */
	public Autorzy read(String nameSurname) {
		@SuppressWarnings({ "unchecked" })
		List<Object> author = session
				.createSQLQuery("SELECT id_autor FROM autorzy WHERE imie_nazwisko='" + nameSurname + "'").list();
		if (author.isEmpty())
			return new Autorzy();
		BigInteger bi = (BigInteger) author.get(0);
		Autorzy autor = session.get(Autorzy.class, bi.longValue());
		return autor;
	}

	/**
	 * Metoda update - poprawia wpis do bazy bez podanego id. Podane są tylko
	 * poniższe parametry. Metoda wywołuje metode  update,
	 * uwzględniającą id, przypisując do id=-1
	 *
	 * @param oldNameSurname the old name surname
	 * @param newNameSurname the new name surname
	 * @return true, if successful
	 */
	public boolean update(String oldNameSurname, String newNameSurname) {
		return update(oldNameSurname, newNameSurname, -1);
	}

	/**
	 * Metoda update - poprawia wpis do bazy. Wywoływana przy użyciu poniższych
	 * parametrów.
	 *
	 * @param oldNameSurname the old name surname
	 * @param newNameSurname the new name surname
	 * @param id the id
	 * @return true, if successful
	 */
	public boolean update(String oldNameSurname, String newNameSurname, long id) {
		Autorzy autor = (id != -1) ? read(id) : read(oldNameSurname);
		if (autor == null)
			return false;
		System.out.println(autor.getNameSurname());
		autor.setNameSurname(newNameSurname);
		session.beginTransaction();
		session.update(autor);
		session.getTransaction().commit();
		return true;
	}

	
	/**
	 * Metoda delete - Podany jest tylko jeden parametr (String). Wywołuje nie
	 * publiczną metodę przy użyciu podanego stringa i przypisując id=-1
	 *
	 * @param nameSurname the name surname
	 * @return true, if successful
	 */
	public boolean delete(String nameSurname) {
		return delete(nameSurname, -1);
	}

	/**
	 * Metoda delete - podany tylko jeden parametr (long id) Wywołuje nie publiczną
	 * metodę przy użyciu pustego stringa i podanego id.
	 *
	 * @param id the id
	 * @return true, if successful
	 */
	public boolean delete(long id) {
		return delete("", id);
	}

	/**
	 * Metoda delete - do której się odwołują wszystkie pozostałe. Metoda ta
	 * uwzględnia wszystkie możliwe parametry.
	 *
	 * @param nameSurname the name surname
	 * @param id the id
	 * @return true, if successful
	 */
	boolean delete(String nameSurname, long id) {
		Autorzy autor = (id != -1) ? read(id) : read(nameSurname);
		if (autor == null)
			return false;
		session.beginTransaction();
		session.delete(autor);
		session.getTransaction().commit();
		return true;
	}

	/**
	 * Metoda -getAll powoduje utworzenie listy zawierającej wszystkie elementy
	 * odczytane z bazy danych. Lista zawiera wskazniki (adresy w pamięci) do
	 * poszczególnych objektów znajdujących się w liście.
	 * 
	 * Jeśli chcemy wyświetlić autorów należy użyć pętli np for.
	 * 
	 * Przykład: for (Autorzy element : autorzyMrun.getAll()) {
	 * System.out.println("Autor: "+ element.getNameSurname());}
	 *
	 * @return the all
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Autorzy> getAll() {
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery criteria = builder.createQuery(Autorzy.class);
		criteria.from(Autorzy.class);
		List<Autorzy> autor = session.createQuery(criteria).getResultList();
		return autor;
	}

}

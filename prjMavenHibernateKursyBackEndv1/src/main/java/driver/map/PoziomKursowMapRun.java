/*
 * 
 */
package driver.map;

import java.math.BigInteger;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import org.hibernate.Session;
import hibernate.maps.*;

// TODO: Auto-generated Javadoc
/**
 * The Class PoziomKursowMapRun.
 *
 * @author adakus
 */

public class PoziomKursowMapRun {

	/** The session. */
	private Session session;

	/**
	 * Instantiates a new poziom kursow map run.
	 *
	 * @param session the session
	 */
	public PoziomKursowMapRun(Session session) {
		this.session = session;
	}

	/**
	 * Metoda - Zapis Poziomu do bazy danych. Należy podać poniższy parametr.
	 *
	 * @param level the level
	 */
	public void save(String level) {
		session.beginTransaction();
		PoziomKursow poziom = new PoziomKursow();
		poziom.setLevel(level);
		session.save(poziom);
		session.getTransaction().commit();
	}

	/**
	 * Metoda - Odczyt Poziomu po przypisanym id. Należy podać poniższy parametr.
	 *
	 * @param id the id
	 * @return the poziom kursow
	 */
	public PoziomKursow read(long id) {
		PoziomKursow poziom = session.get(PoziomKursow.class, id);
		return poziom;
	}

	/**
	 * Metoda - odczyt Poziomu po wartości. Należy podać poniższy parametr.
	 *
	 * @param level the level
	 * @return the poziom kursow
	 */
	public PoziomKursow read(String level) {
		@SuppressWarnings({ "unchecked" })
		List<Object> lvl = session
				.createSQLQuery("SELECT id_poziom_kursu FROM poziom_kursow WHERE poziom_trudnosci='" + level + "'")
				.list();
		if (lvl.isEmpty())
			return new PoziomKursow();
		BigInteger bi = (BigInteger) lvl.get(0);
		PoziomKursow poziom = session.get(PoziomKursow.class, bi.longValue());
		return poziom;
	}

	/**
	 * Metoda update - poprawia wpis do bazy bez podanego id. Podane są tylko
	 * poniższe parametry. Metoda wywołuje metode nie publiczną update,
	 * uwzględniającą id, przypisując do id=-1
	 *
	 * @param oldLevel the old level
	 * @param newLevel the new level
	 * @return true, if successful
	 */
	public boolean update(String oldLevel, String newLevel) {
		return update(oldLevel, newLevel, -1);
	}

	/**
	 * Metoda update - poprawia wpis do bazy. Wywoływana przy użyciu poniższych
	 * parametrów.
	 *
	 * @param oldLevel the old level
	 * @param newLevel the new level
	 * @param id the id
	 * @return true, if successful
	 */
	boolean update(String oldLevel, String newLevel, long id) {
		PoziomKursow poziom = (id != -1) ? read(id) : read(oldLevel);
		if (poziom == null)
			return false;
		System.out.println(poziom.getLevel());
		poziom.setLevel(newLevel);
		session.beginTransaction();
		session.update(poziom);
		session.getTransaction().commit();
		return true;
	}

	/**
	 * Metoda delete - Podany jest tylko jeden parametr (String). Wywołuje nie
	 * publiczną metodę przy użyciu podanego stringa i przypisując id=-1
	 *
	 * @param level the level
	 * @return true, if successful
	 */
	public boolean delete(String level) {
		return delete(level, -1);
	}

	/**
	 * Metoda delete - podany tylko jeden parametr (long id) Wywołuje nie publiczną
	 * metodę przy użyciu pustego stringa i podanego id.
	 *
	 * @param id the id
	 * @return true, if successful
	 */
	public boolean delete(long id) {
		return delete("", id);
	}

	/**
	 * Metoda delete - do której się odwołują wszystkie pozostałe. Metoda ta
	 * uwzględnia wszystkie możliwe parametry.
	 *
	 * @param level the level
	 * @param id the id
	 * @return true, if successful
	 */
	boolean delete(String level, long id) {
		PoziomKursow poziom = (id != -1) ? read(id) : read(level);
		if (poziom == null)
			return false;
		session.beginTransaction();
		session.delete(poziom);
		session.getTransaction().commit();
		return true;
	}

	/**
	 * Metoda -getAll powoduje utworzenie listy zawierającej wszystkie elementy
	 * odczytane z bazy danych. Lista zawiera wskazniki (adresy w pamięci) do
	 * poszczególnych objektów znajdujących się w liście.
	 * 
	 * Jeśli chcemy wyświetlić poziomy kursów należy użyć pętli np for.
	 * 
	 * Przykład: for (Uzytkownicy element : uzytkownicyMrun.getAll()) {
	 * System.out.println("Uzytkownik: "+ element.getNameSurname());}
	 *
	 * @return the all
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<PoziomKursow> getAll() {
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery criteria = builder.createQuery(PoziomKursow.class);
		criteria.from(PoziomKursow.class);
		List<PoziomKursow> poziom = session.createQuery(criteria).getResultList();
		return poziom;
	}

}

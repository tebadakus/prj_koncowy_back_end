/*
 * 
 */
package driver.map;

import java.math.BigInteger;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import org.hibernate.Session;
import hibernate.maps.*;

// TODO: Auto-generated Javadoc
/**
 * The Class UzytkownicyMapRun.
 */
public class UzytkownicyMapRun {

	/** The session. */
	private Session session;

	/**
	 * Instantiates a new uzytkownicy map run.
	 *
	 * @param session the session
	 */
	public UzytkownicyMapRun(Session session) {
		this.session = session;
	}

	/**
	 * Metoda -Dodanie uzytkownika potrzebne są wszystkie poniższe parametry.
	 *
	 * @param usrNameSurname the usr name surname
	 * @param email the email
	 * @param phone the phone
	 * @param login the login
	 * @param password the password
	 * @param idCourse the id course
	 */
	public void save(String usrNameSurname, String email, String phone, String login, String password, long idCourse) {
		session.beginTransaction();
		Uzytkownicy uzytkownik = new Uzytkownicy();
		uzytkownik.setUsrNameSurname(usrNameSurname);
		uzytkownik.setEmail(email);
		uzytkownik.setPhone(phone);
		uzytkownik.setLogin(login);
		uzytkownik.setPassword(password);
		uzytkownik.setIdCourse(idCourse);
		session.save(uzytkownik);
		session.getTransaction().commit();
	}

	/**
	 * Metoda - Odczyt Urzytkowwnika po przypisanym id. Należy podać poniższy
	 * parametr.
	 *
	 * @param id the id
	 * @return the uzytkownicy
	 */
	public Uzytkownicy read(long id) {
		Uzytkownicy uzytkownik = session.get(Uzytkownicy.class, id);
		return uzytkownik;
	}

	/**
	 * Metoda - odczyt danych Uzytkownika po podanych danych z tabeli. Należy podać poniższe
	 * parametry. 
	 *
	 * @param usrData the usr data
	 * @param usrColumn the usr column
	 * @return the uzytkownicy
	 */
	public Uzytkownicy read(String usrData, String usrColumn) {
		@SuppressWarnings({ "unchecked" })
		List<Object> user = session
				.createSQLQuery("SELECT id_uzytkownik FROM uzytkownicy WHERE "+ usrColumn+"= '" + usrData + "'")
				.list();
		if (user.isEmpty())
			return new Uzytkownicy();
		BigInteger bi = (BigInteger) user.get(0);
		Uzytkownicy uzytkownik = session.get(Uzytkownicy.class, bi.longValue());
		return uzytkownik;
	}

	/**
	 * Metoda - odczyt Urzytkownika po imieniu i nazwisku. Należy podać poniższy
	 * parametr. Podajemy imie i nazwisko oddzielone spacją np. "Adam Mickiewicz"
	 *
	 * @param usrNameSurname the usr name surname
	 * @return the uzytkownicy
	 */
	public Uzytkownicy read(String usrNameSurname) {
		@SuppressWarnings({ "unchecked" })
		List<Object> user = session
				.createSQLQuery("SELECT id_uzytkownik FROM uzytkownicy WHERE imie_nazwisko='" + usrNameSurname + "'")
				.list();
		if (user.isEmpty())
			return new Uzytkownicy();
		BigInteger bi = (BigInteger) user.get(0);
		Uzytkownicy uzytkownik = session.get(Uzytkownicy.class, bi.longValue());
		return uzytkownik;
	}
	
	
	/**
	 * Metoda update - poprawia wpis do bazy bez podanego id. Podane są tylko
	 * poniższe parametry. Metoda wywołuje metode update, uwzględniającą id,
	 * przypisując do id=-1
	 *
	 * @param column the column
	 * @param oldString the old string
	 * @param newString the new string
	 * @return true, if successful
	 */
	public boolean update(String column, String oldString, String newString) {
		return update(column, oldString, newString, -1);
	}
	
	/**
	 * Metoda wywoływana jako metoda uwzgledniająca podane id uzytkownika.
	 * Pozwala na update kolumn z poszczególnych kolumn: imie_nazwisko, email, telefon, login. 
	 *
	 * @param column the column
	 * @param oldString the old string
	 * @param newString the new string
	 * @param id the id
	 * @return true, if successful
	 */

	boolean update(String column, String oldString, String newString, long id) {
		Uzytkownicy uzytkownik = (id != -1) ? read(id) : read(oldString,column);
		if (uzytkownik == null)
			return false;

		if (column.equals("imie_nazwisko")) {
			System.out.println(uzytkownik.getUsrNameSurname());
			uzytkownik.setUsrNameSurname(newString);
		}

		if (column.equals("email")) {
			System.out.println(uzytkownik.getEmail());
			uzytkownik.setEmail(newString);
		}
		
		if (column.equals("telefon")) {
			System.out.println(uzytkownik.getPhone());
			uzytkownik.setPhone(newString);
		}
		if (column.equals("login")) {
			System.out.println(uzytkownik.getLogin());
			uzytkownik.setLogin(newString);
		}
		session.beginTransaction();
		session.update(uzytkownik);
		session.getTransaction().commit();
		return true;
	}
	
	/**
	 * Metoda update - poprawia wpis do bazy. Wywoływana przy użyciu poniższych
	 * parametrów.
	 *
	 * @param column the column
	 * @param row the row
	 * @param oldId the old id
	 * @param newId the new id
	 * @return true, if successful
	 */
	public boolean update(String column, String row, long oldId, long newId) {
		return update(column, row, oldId, newId, -1);
	}
	
	/**
	 * Metoda update - poprawia wpis do bazy. Wywoływana przy użyciu poniższych
	 * parametrów z uwzględnieniem id uzytkownika.
	 *
	 * @param column the column
	 * @param row the row
	 * @param oldId the old id
	 * @param newId the new id
	 * @param id the id
	 * @return true, if successful
	 */
	boolean update(String column, String row, long oldId, long newId, long id) {
		Uzytkownicy uzytkownik = (id != -1) ? read(id) : read(row);
		if (uzytkownik == null)
			return false;

		if (column.equals("id_kursu")) {
			System.out.println(uzytkownik.getIdCourse());
			uzytkownik.setIdCourse(newId);
		}

		
		session.beginTransaction();
		session.update(uzytkownik);
		session.getTransaction().commit();
		return true;
	}
	
	
	/**
	 * Metoda update - Zmiana hasla urzytkownika bez podanego id. Podane są tylko
	 * poniższe parametry. Metoda wywołuje metode update, uwzględniającą id,
	 * przypisując do id=-1
	 *
	 * @param oldUsrPassword the old usr password
	 * @param newUsrPassword the new usr password
	 * @param newUsrPasswordConfirm the new usr password confirm
	 * @param usrNameSurname the usr name surname
	 * @return true, if successful
	 */
	public boolean update(String oldUsrPassword, String newUsrPassword, String newUsrPasswordConfirm,
			String usrNameSurname) {
		return update(oldUsrPassword, newUsrPassword, newUsrPasswordConfirm, usrNameSurname, -1);
	}

	/**
	 * Metoda update - Zmienia hasło uzytkownika. Wywoływana przy użyciu poniższych
	 * parametrów.
	 *
	 * @param oldUsrPassword the old usr password
	 * @param newUsrPassword the new usr password
	 * @param newUsrPasswordConfirm the new usr password confirm
	 * @param usrNameSurname the usr name surname
	 * @param id the id
	 * @return true, if successful
	 */
	boolean update(String oldUsrPassword, String newUsrPassword, String newUsrPasswordConfirm, String usrNameSurname,
			long id) {
		if (!(newUsrPassword.equals(newUsrPasswordConfirm)))
			return false;

		Uzytkownicy uzytkownik = (id != -1) ? read(id) : read(oldUsrPassword);
		if (uzytkownik == null)
			return false;
		if (!(uzytkownik.getUsrNameSurname().equals(usrNameSurname)))
			return false;
		System.out.println(uzytkownik.getUsrNameSurname());

		uzytkownik.setPassword(newUsrPassword);
		session.beginTransaction();
		session.update(uzytkownik);
		session.getTransaction().commit();
		return true;
	}

	/**
	 * Metoda delete - Podany jest tylko jeden parametr (String). Wywołuje metodę
	 * delete przy użyciu podanego stringa i przypisując id=-1
	 *
	 * @param usrNameSurname the usr name surname
	 * @return true, if successful
	 */
	public boolean delete(String usrNameSurname) {
		return delete(usrNameSurname, -1);
	}

	/**
	 * Metoda delete - podany tylko jeden parametr (long id) Wywołuje metodę delete
	 * przy użyciu pustego stringa i podanego id.
	 *
	 * @param id the id
	 * @return true, if successful
	 */
	public boolean delete(long id) {
		return delete("", id);
	}

	/**
	 * Metoda delete - do której się odwołują wszystkie pozostałe. Metoda ta
	 * uwzględnia wszystkie możliwe parametry.
	 *
	 * @param usrNameSurname the usr name surname
	 * @param id the id
	 * @return true, if successful
	 */
	boolean delete(String usrNameSurname, long id) {
		Uzytkownicy uzytkownik = (id != -1) ? read(id) : read(usrNameSurname);
		if (uzytkownik == null)
			return false;
		session.beginTransaction();
		session.delete(uzytkownik);
		session.getTransaction().commit();
		return true;
	}

	/**
	 * Metoda -getAll powoduje utworzenie listy zawierającej wszystkie elementy
	 * odczytane z bazy danych. Lista zawiera wskazniki (adresy w pamięci) do
	 * poszczególnych objektów znajdujących się w liście.
	 * 
	 * Jeśli chcemy wyświetlić autorów należy użyć pętli np for.
	 * 
	 * Przykład: for (Uzytkownicy element : uzytkownicyMrun.getAll()) {
	 * System.out.println("Uzytkownik: "+ element.getNameSurname());}
	 *
	 * @return the all
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Uzytkownicy> getAll() {
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery criteria = builder.createQuery(Uzytkownicy.class);
		criteria.from(Uzytkownicy.class);
		List<Uzytkownicy> uzytkownik = session.createQuery(criteria).getResultList();
		return uzytkownik;
	}

}

/*
 * 
 */
package driver.map;

/**
 * @author adakus
 *
 */

import java.math.BigInteger;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import org.hibernate.Session;
import hibernate.maps.*;

// TODO: Auto-generated Javadoc
/**
 * The Class RodzajeKursowMapRun.
 */
public class RodzajeKursowMapRun {

	/** The session. */
	private Session session;

	/**
	 * Instantiates a new rodzaje kursow map run.
	 *
	 * @param session the session
	 */
	public RodzajeKursowMapRun(Session session) {
		this.session = session;
	}

	/**
	 * Metoda - Zapis RodzajuKursu do bazy danych. Należy podać poniższy parametr.
	 * np. tekstowy, video, kulinarny, programowanie, itd
	 *
	 * @param courseType the course type
	 */
	public void save(String courseType) {
		session.beginTransaction();
		RodzajeKursow kursTyp = new RodzajeKursow();
		kursTyp.setCourseType(courseType);
		session.save(kursTyp);
		session.getTransaction().commit();
	}

	/**
	 * Metoda - Odczyt Rodzaju Kursu po przypisanym id. Należy podać poniższy
	 * parametr.
	 *
	 * @param id the id
	 * @return the rodzaje kursow
	 */
	public RodzajeKursow read(long id) {
		RodzajeKursow kursTyp = session.get(RodzajeKursow.class, id);
		return kursTyp;
	}

	/**
	 * Metoda - odczyt RodzajuKursu po typie kursu. Należy podać poniższy parametr.
	 * Podajemy typ kursu np. "tekstowy, kulinarny, programowanie, itd"
	 *
	 * @param courseType the course type
	 * @return the rodzaje kursow
	 */
	public RodzajeKursow read(String courseType) {
		@SuppressWarnings({ "unchecked" })
		List<Object> courseT = session
				.createSQLQuery("SELECT id_rodzaj_kursu FROM rodzaje_kursow WHERE rodzaj_kursu='" + courseType + "'")
				.list();
		if (courseT.isEmpty())
			return new RodzajeKursow();
		BigInteger bi = (BigInteger) courseT.get(0);
		RodzajeKursow kursTyp = session.get(RodzajeKursow.class, bi.longValue());
		return kursTyp;
	}

	/**
	 * Metoda update - poprawia wpis do bazy bez podanego id. Podane są tylko
	 * poniższe parametry. Metoda wywołuje metode nie publiczną update,
	 * uwzględniającą id, przypisując do id=-1
	 *
	 * @param oldCourseType the old course type
	 * @param newCourseType the new course type
	 * @return true, if successful
	 */
	public boolean update(String oldCourseType, String newCourseType) {
		return update(oldCourseType, newCourseType, -1);
	}

	/**
	 * Metoda update - poprawia wpis do bazy. Wywoływana przy użyciu poniższych
	 * parametrów.
	 *
	 * @param oldCourseType the old course type
	 * @param newCourseType the new course type
	 * @param id the id
	 * @return true, if successful
	 */
	boolean update(String oldCourseType, String newCourseType, long id) {
		RodzajeKursow kursTyp = (id != -1) ? read(id) : read(oldCourseType);
		if (kursTyp == null)
			return false;
		System.out.println(kursTyp.getCourseType());
		kursTyp.setCourseType(newCourseType);
		session.beginTransaction();
		session.update(kursTyp);
		session.getTransaction().commit();
		return true;
	}

	/**
	 * Metoda delete - Podany jest tylko jeden parametr (String). Wywołuje nie
	 * publiczną metodę przy użyciu podanego stringa i przypisując id=-1
	 *
	 * @param courseType the course type
	 * @return true, if successful
	 */
	public boolean delete(String courseType) {
		return delete(courseType, -1);
	}

	/**
	 * Metoda delete - podany tylko jeden parametr (long id) Wywołuje nie publiczną
	 * metodę przy użyciu pustego stringa i podanego id.
	 *
	 * @param id the id
	 * @return true, if successful
	 */
	public boolean delete(long id) {
		return delete("", id);
	}

	/**
	 * Metoda delete - do której się odwołują wszystkie pozostałe. Metoda ta
	 * uwzględnia wszystkie możliwe parametry.
	 *
	 * @param courseType the course type
	 * @param id the id
	 * @return true, if successful
	 */
	boolean delete(String courseType, long id) {
		RodzajeKursow kursTyp = (id != -1) ? read(id) : read(courseType);
		if (kursTyp == null)
			return false;
		session.beginTransaction();
		session.delete(kursTyp);
		session.getTransaction().commit();
		return true;
	}

	/**
	 * Metoda -getAll powoduje utworzenie listy zawierającej wszystkie elementy
	 * odczytane z bazy danych. Lista zawiera wskazniki (adresy w pamięci) do
	 * poszczególnych objektów znajdujących się w liście.
	 * 
	 * Jeśli chcemy wyświetlić elementy należy użyć pętli np for.
	 * 
	 * Przykład: for (RodzajeKursow element : rodzajeKursowMrun.getAll()) {
	 * System.out.println("Rodzaj Kursu: "+ element.getCourseType());}
	 *
	 * @return the all
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<RodzajeKursow> getAll() {
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery criteria = builder.createQuery(RodzajeKursow.class);
		criteria.from(RodzajeKursow.class);
		List<RodzajeKursow> typyKursow = session.createQuery(criteria).getResultList();
		return typyKursow;
	}

}

/*
 * 
 */
package hibernate.maps;

// TODO: Auto-generated Javadoc
/**
 * The Class Uzytkownicy.
 */
public class Uzytkownicy {
	
	/** The id. */
	private long id;
	
	/** The usr name surname. */
	private String usrNameSurname;
	
	/** The email. */
	private String email;
	
	/** The phone. */
	private String phone;
	
	/** The id course. */
	private long idCourse;
	
	/** The login. */
	private String login;
	
	/** The password. */
	private String password;
	
	
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	
	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(long id) {
		this.id = id;
	}
	
	/**
	 * Gets the usr name surname.
	 *
	 * @return the usr name surname
	 */
	public String getUsrNameSurname() {
		return usrNameSurname;
	}
	
	/**
	 * Sets the usr name surname.
	 *
	 * @param usrNameSurname the new usr name surname
	 */
	public void setUsrNameSurname(String usrNameSurname) {
		this.usrNameSurname = usrNameSurname;
	}
	
	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	
	/**
	 * Sets the email.
	 *
	 * @param email the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	/**
	 * Gets the phone.
	 *
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}
	
	/**
	 * Sets the phone.
	 *
	 * @param phone the new phone
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	/**
	 * Gets the id course.
	 *
	 * @return the id course
	 */
	public long getIdCourse() {
		return idCourse;
	}
	
	/**
	 * Sets the id course.
	 *
	 * @param idCourse the new id course
	 */
	public void setIdCourse(long idCourse) {
		this.idCourse = idCourse;
	}
	
	/**
	 * Gets the login.
	 *
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}
	
	/**
	 * Sets the login.
	 *
	 * @param login the new login
	 */
	public void setLogin(String login) {
		this.login = login;
	}
	
	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	
	/**
	 * Sets the password.
	 *
	 * @param password the new password
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
	
}

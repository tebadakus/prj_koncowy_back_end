/*
 * 
 */
package hibernate.maps;

// TODO: Auto-generated Javadoc
/**
 * The Class Kursy.
 */
public class Kursy {
	
	/** The id. */
	private long id;
	
	/** The course name. */
	private String courseName;
	
	/** The date added. */
	private String dateAdded;
	
	/** The id course type. */
	private long idCourseType;
	
	/** The id course level. */
	private long idCourseLevel;
	
	/** The id author. */
	private long idAuthor;
	
	/** The id assets. */
	private long idAssets;
	
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	
	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(long id) {
		this.id = id;
	}
	
	/**
	 * Gets the course name.
	 *
	 * @return the course name
	 */
	public String getCourseName() {
		return courseName;
	}
	
	/**
	 * Sets the course name.
	 *
	 * @param courseName the new course name
	 */
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	
	/**
	 * Gets the date added.
	 *
	 * @return the date added
	 */
	public String getDateAdded() {
		return dateAdded;
	}
	
	/**
	 * Sets the date added.
	 *
	 * @param dateAdded the new date added
	 */
	public void setDateAdded(String dateAdded) {
		this.dateAdded = dateAdded;
	}
	
	/**
	 * Gets the id course type.
	 *
	 * @return the id course type
	 */
	public long getIdCourseType() {
		return idCourseType;
	}
	
	/**
	 * Sets the id course type.
	 *
	 * @param idCourseType the new id course type
	 */
	public void setIdCourseType(long idCourseType) {
		this.idCourseType = idCourseType;
	}
	
	/**
	 * Gets the id course level.
	 *
	 * @return the id course level
	 */
	public long getIdCourseLevel() {
		return idCourseLevel;
	}
	
	/**
	 * Sets the id course level.
	 *
	 * @param idCourseLevel the new id course level
	 */
	public void setIdCourseLevel(long idCourseLevel) {
		this.idCourseLevel = idCourseLevel;
	}
	
	/**
	 * Gets the id author.
	 *
	 * @return the id author
	 */
	public long getIdAuthor() {
		return idAuthor;
	}
	
	/**
	 * Sets the id author.
	 *
	 * @param idAuthor the new id author
	 */
	public void setIdAuthor(long idAuthor) {
		this.idAuthor = idAuthor;
	}
	
	/**
	 * Gets the id assets.
	 *
	 * @return the id assets
	 */
	public long getIdAssets() {
		return idAssets;
	}
	
	/**
	 * Sets the id assets.
	 *
	 * @param idAssets the new id assets
	 */
	public void setIdAssets(long idAssets) {
		this.idAssets = idAssets;
	}
	
}

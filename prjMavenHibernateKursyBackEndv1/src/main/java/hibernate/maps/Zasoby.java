/*
 * 
 */
package hibernate.maps;

// TODO: Auto-generated Javadoc
/**
 * The Class Zasoby.
 */
public class Zasoby {
	
	/** The id. */
	private long id;
	
	/** The asset. */
	private String asset;
	
	
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	
	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(long id) {
		this.id = id;
	}
	
	/**
	 * Gets the asset.
	 *
	 * @return the asset
	 */
	public String getAsset() {
		return asset;
	}
	
	/**
	 * Sets the asset.
	 *
	 * @param asset the new asset
	 */
	public void setAsset(String asset) {
		this.asset = asset;
	}
	
	
}

/*
 * 
 */
package hibernate.maps;

// TODO: Auto-generated Javadoc
/**
 * The Class PoziomKursow.
 */
public class PoziomKursow {
	
	/** The id. */
	private long id;
	
	/** The level. */
	private String level;
	
	
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	
	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(long id) {
		this.id = id;
	}
	
	/**
	 * Gets the level.
	 *
	 * @return the level
	 */
	public String getLevel() {
		return level;
	}
	
	/**
	 * Sets the level.
	 *
	 * @param level the new level
	 */
	public void setLevel(String level) {
		this.level = level;
	}
	
	
}

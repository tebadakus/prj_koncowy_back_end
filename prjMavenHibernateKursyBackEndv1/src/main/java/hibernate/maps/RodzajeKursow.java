/*
 * 
 */
package hibernate.maps;

// TODO: Auto-generated Javadoc
/**
 * The Class RodzajeKursow.
 */
public class RodzajeKursow {
	
	/** The id. */
	private long id;
	
	/** The course type. */
	private String courseType;
	
	
	
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	
	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(long id) {
		this.id = id;
	}
	
	/**
	 * Gets the course type.
	 *
	 * @return the course type
	 */
	public String getCourseType() {
		return courseType;
	}
	
	/**
	 * Sets the course type.
	 *
	 * @param courseType the new course type
	 */
	public void setCourseType(String courseType) {
		this.courseType = courseType;
	}
	
	
	
}

/*
 * 
 */
package hibernate.maps;

// TODO: Auto-generated Javadoc
/**
 * The Class Autorzy.
 */
public class Autorzy {
	
	/** The id. */
	private long id;
	
	/** The name surname. */
	private String nameSurname; //imie_nazwisko
	
	
	
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	
	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(long id) {
		this.id = id;
	}
	
	/**
	 * Gets the name surname.
	 *
	 * @return the name surname
	 */
	public String getNameSurname() {
		return nameSurname;
	}
	
	/**
	 * Sets the name surname.
	 *
	 * @param nameSurname the new name surname
	 */
	public void setNameSurname(String nameSurname) {
		this.nameSurname = nameSurname;
	}
	
}
